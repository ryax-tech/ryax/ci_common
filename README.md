# Ryax Continuous Integration and Delivery Common Material
This repository contains common steps to be included by the CI of each Ryax
projects.

## Update Template for CI

To update the default Nix recipe and Gitlab config use:
```bash
nix flake init -t nix flake init -t "git+https://gitlab.com/ryax-tech/ryax/ci_common
```

Change the `CHANGEME` values.

## How it works

This repository is injecting intself in the CI with these lines in the  `.gitlab-ci.yml` file (see ./nix/template/.gitlab-ci.yml):
```yaml
include:
  - project: 'ryax-tech/ryax/ci_common'
    ref: "4.0.0"
    file: 'common-steps-gitlab-ci.yml'

# Usage Example
check_code_quality:
  extends: .check_code_quality
  variables:
    LINT_CMD: ./mylint.sh

verify_helm_charts:
  extends: .verify_helm_charts
```

## Build the Ryax CI container image

Requirements:
- Have nix installed
- Have Qemu Binfmt working for other architecture (amd64 and arm64). On NixOS
  just add `boot.binfmt.emulatedSystems = [ "aarch64-linux" ];`

Choose a version. The format is `<Nixpkgs version>-<Increasing integer>`
```sh
./update-image.sh 23.11-0
```
This script will build and push the two images and then create the multi-arch manifest.
