{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    flakeUtils.follows = "nix2container/flake-utils";
  };

  outputs = { self, nixpkgs, flakeUtils, nix2container }:
    rec {
      templates = {
        ryaxService = {
          path = ./nix/template;
          description = "Ryax Service default template";
        };
      };
      templates.default = templates.ryaxService;
    } // (
      flakeUtils.lib.eachDefaultSystem (system:
        let
          pkgs = import nixpkgs { inherit system; };
          nix2containerPkgs = nix2container.packages.${system};
        in
        {
          # Enable autoformat
          formatter = pkgs.nixpkgs-fmt;
          # Container image for Ryax CI
          packages.ci-image = import ./nix/ci-image.nix {
            nix2container = nix2containerPkgs.nix2container;
            inherit pkgs;
          };
        }
      )
    );
}
