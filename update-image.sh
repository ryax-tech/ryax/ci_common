#/usr/bin/env bash

set -e
set -x
set -u

export VERSION=$1

nix run .\#ci-image.copyTo docker://docker.io/ryaxtech/ryax-ci-amd64:$VERSION
nix run --system aarch64-linux .\#ci-image.copyTo docker://docker.io/ryaxtech/ryax-ci-arm64:$VERSION
nix run nixpkgs\#manifest-tool -- push from-args --platforms linux/amd64,linux/arm64 --template ryaxtech/ryax-ci-ARCH:$VERSION --target ryaxtech/ryax-ci:$VERSION

