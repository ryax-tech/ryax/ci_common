{ nix2container
, pkgs
, python ? pkgs.python3
, tag ? "latest"
}:
let
  user = "root";
  group = "root";
  uid = 0;
  gid = 0;
  uidStr = builtins.toString uid;
  gidStr = builtins.toString gid;
  userHome = "/root";

  wrapperDir = "/run/wrappers/bin";
  parentWrapperDir = dirOf wrapperDir;
  program = "singularity-suid";
  singularityConfig =
    let
      # Migration data must go into the `data` directory which is the working dir in the container.
      source = "${singularity-modified}/libexec/singularity/bin/starter-suid.orig";
      securityWrapper = pkgs.callPackage ./wrapper.nix {
        inherit parentWrapperDir;
      };
    in
    runCommand "singularity-setup" { } ''
      set -x
      mkdir -p $out/etc
      mkdir -p $out/root

      # Make Singularity an suid executable
      wrapperDir=$out${wrapperDir}
      mkdir -p $wrapperDir
      cp ${securityWrapper}/bin/security-wrapper "$wrapperDir/${program}"
      echo -n "${source}" > "$wrapperDir/${program}.real"
      ls -al "$wrapperDir/${program}"

      mkdir -m 0770 -p $out/var/singularity/mnt/session
      mkdir -m 0770 -p $out/var/singularity/mnt/final
      mkdir -m 0770 -p $out/var/singularity/mnt/overlay
      mkdir -m 0770 -p $out/var/singularity/mnt/container
      mkdir -m 0770 -p $out/var/singularity/mnt/source

      cat ${./localtime} > $out/etc/localtime
      cat > $out/etc/subuid <<EOF
      ryax:165536:65536
      EOF
      cat > $out/etc/subgid <<EOF
      ryax:165536:65536
      EOF
    '';
  singularity-modified = pkgs.singularity.override ({ enableSuid = true; starterSuidPath = "/run/wrappers/bin/singularity-suid"; });
  extraTools = nix2container.buildLayer {
    copyToRoot =
      [
        (pkgs.buildEnv {
          name = "root";
          paths = with pkgs; [ singularity-modified skopeo pkgs.gnutar shadow libuuid ];
          pathsToLink = [ "/bin" ];
        })
        singularityConfig
      ];
    perms = [
      {
        path = singularityConfig;
        regex = "/var/singularity/mnt/*";
        mode = "0770";
      }
      {
        path = singularityConfig;
        regex = parentWrapperDir;
        mode = "755";
      }
      {
        path = singularityConfig;
        regex = wrapperDir;
        mode = "755";
      }
      {
        path = singularityConfig;
        regex = "${wrapperDir}/${program}";
        mode = "4755";
        uid = 0;
        gid = 0;
        uname = "root";
        gname = "root";
      }
    ];
   };

  inherit (pkgs) runCommand cacert coreutils;
  defaultConfig = runCommand "config" { } ''
    # add cachix config
    mkdir -p $out${userHome}/.config/nix/
    cat > $out${userHome}/.config/nix/nix.conf <<EOF
    experimental-features = nix-command flakes
    substituters = https://cache.nixos.org https://ryaxtech-ci.cachix.org
    trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= ryaxtech-ci.cachix.org-1:NrMfvWdMz91z1V3PZX0qcLbhMRjDRP4XOncQ1T57wjQ=
    EOF

    mkdir -p $out/etc/ssl/certs/
    ln -s ${cacert}/etc/ssl/certs/ca-bundle.crt $out/etc/ssl/certs/ca-certificates.crt

    # Create temporary directories
    mkdir $out/tmp
    mkdir -p $out/var/tmp
    mkdir -p $out/var/cache/ccache

    mkdir -p $out/usr/bin
    ln -s ${coreutils}/bin/env $out/usr/bin/env

    # Fix localhost DNS resolution
    cat > $out/etc/nsswitch.conf <<EOF
    hosts: files dns
    EOF

    # create the root user
    mkdir -p $out/etc/pam.d
    echo "root:x:0:0:Ryax User:root:/bin/bash" > $out/etc/passwd
    echo "root:!x:::::::" > $out/etc/shadow
    echo "root:x:0:" > $out/etc/group
    echo "root:x::" > $out/etc/gshadow

    # Setup pam support
    cat >> $out/etc/pam.d/other <<EOF
    account sufficient pam_unix.so
    auth sufficient pam_rootok.so
    password requisite pam_unix.so nullok sha512
    session required pam_unix.so
    EOF
    touch $out/etc/login.defs

    # create the Root user
    echo "${user}:x:${uidStr}:${gidStr}:Admin user:${userHome}:/bin/bash" >> $out/etc/passwd
    echo "${user}:!x:::::::" >> $out/etc/shadow
    echo "${group}:x:${gidStr}:" >> $out/etc/group
    echo "${group}:x::" >> $out/etc/gshadow

    mkdir -p $out${userHome}

    # Create nixbld user
    echo "nixbld:x:30000:30000:Nix Build user:/var/empty:/bin/nologin" >> $out/etc/passwd
    echo "nixbld:!x:::::::" >> $out/etc/shadow
    echo "nixbld:x:30000:nixbld" >> $out/etc/group
    echo "nixbld:x::" >> $out/etc/gshadow
  '';
in
nix2container.buildImage {
  name = "ryax-ci";
  inherit tag;

  perms = [
    {
      path = defaultConfig;
      regex = "/tmp";
      mode = "1777";
    }
    {
      path = defaultConfig;
      regex = "/var/tmp";
      mode = "1777";
    }
    {
      path = defaultConfig;
      regex = "/var/cache/ccache";
      mode = "0770";
      uid = uid;
      gid = gid;
      uname = user;
      gname = group;
    }
    {
      path = defaultConfig;
      regex = userHome;
      mode = "0744";
      uid = uid;
      gid = gid;
      uname = user;
      gname = group;
    }
  ];

  copyToRoot =
    let
      pythonWithPackages = python.withPackages (ps: with ps; [ pip setuptools ]);
    in
    [
      (pkgs.buildEnv {
        name = "root";
        paths = with pkgs; [
          coreutils
          gitMinimal
          pythonWithPackages
          cachix
          nix
          bashInteractive
          findutils
          which
          procps
          gnutar
          gnused
          gnugrep
          gnumake
          gcc
          poetry
          # For docker compose use for testing
          docker
          docker-compose
          podman
          skopeo
          singularity
          # To push multi-arch manifest
          manifest-tool
          # For adm testing
          kubectl
          # For release
          jq
          yq
          curl
          moreutils
        ];
        pathsToLink = [ "/bin" ];
      })
      defaultConfig
    ];

  initializeNixDatabase = true;
  nixUid = uid;
  nixGid = gid;

  config.User = user;

  config.EntryPoint = [ "/bin/bash" "-c" ];
  config.Env = [
    "NIX_PAGER=cat"
    "ENV=/etc/profile"
    "USER=${user}"
    "HOME=${userHome}"
  ];
  config.WorkingDir = userHome;
  config.Labels = {
    "ryax.tech" = "ryax-ci";
  };
}
